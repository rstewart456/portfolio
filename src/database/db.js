import bhc from '../assets/bhc.png';
import rc from '../assets/rc.png';
import rn from '../assets/rn.png';
import bns from '../assets/bns.png';

const db = [
	{
		"id": 		1,
		"name": 	"Bootstrap Course",
		"image": 	bhc,
		"text": 	"The First courses in the NuCamp Boot Camp. It uses Html, Css, Bootstrap and jQuerry . The website uses about reserving one of their campgrounds.",
		"source":	"https://gitlab.com/rstewart456/Nucamp-Bootstrap"
	},
	{
		"id":			2,
		"name": 	"React Course",
		"image": 	rc,
		"text":		"This is the Second Course from NuCamp. It takes the first course and converts over to React. It uses React, Reactstrap, Bootstrap and Redux. It adds extra like making comments on your Campground experience and adding favorite Campgrounds.",
		"source": "https://gitlab.com/rstewart456/Nucamp-React"
	},
	{
		"id":			3,
		"name":		"React Native Course",
		"image": 	rn,
		"text":		"This is the Third Course from NuCamp. It takes from the second course and converts it into a React Native App. It uses React Native, React Navigation, React Redux,  This course adds sign in or login. The app allows you to update your login picture.",
		"source":	"https://gitlab.com/rstewart456/Nucamp-ReactNative"
	},
	{
		"id":			4,
		"name":		"Back In Server",
		"image": 	bns,
		"text":		"This is the Fourth course from NuCamp. This Course uses Express Server, Mongoose and MongoDb.",
		"source":	"https://gitlab.com/rstewart456/nucampserver"
	}
]

export default db;
