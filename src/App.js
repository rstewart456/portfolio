import "./App.css";
import Navbar from "./components/navbar/navbar";
import Home from "./components/home/home";
import Projects from "./components/projects/projects";
import Resume from "./components/resume/resume";
import Contact from "./components/contact/contact";

function App() {
  return (
    <div className="App vw-100">
      <Navbar />
      <Home />
      <Projects />
      <Resume />
      <Contact />
    </div>
  );
}

export default App;
