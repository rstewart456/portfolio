import "./projects.css";
import React, { useEffect } from "react";
import { Card, Button, Row, Col } from "react-bootstrap";
import { useInView } from "react-intersection-observer";
import { motion, useAnimation } from "framer-motion";
import db from "../../database/db";

function ProjectCard({ key, name, image, text, source }) {
  const animation = useAnimation();
  const [ref, inView] = useInView({ threshold: 0.1 });

  useEffect(() => {
    if (inView) {
      animation.start("visiable");
    } else {
      animation.start("hidden");
    }
  });

  return (
    <Col className="py-5 d-flex justify-content-center align-items-center">
      <motion.div
        ref={ref}
        animate={inView ? { opacity: 1 } : { opacity: 0 }}
        transition={{ duration: 1.2 }}
        initial="hidden"
      >
        <Card className="cardCss">
          <Card.Img variant="top" style={{ height: "200px" }} src={image} />
          <Card.Body>
            <Card.Title>{name}</Card.Title>
            <Card.Text style={{ height: "200px" }}>{text}</Card.Text>
            <Button
              variant="primary"
              href={source}
              target="_blank"
              rel="noreferrer noopener"
            >
              Source Code
            </Button>
          </Card.Body>
        </Card>
      </motion.div>
    </Col>
  );
}

function Projects() {
  return (
    <div className="projectCss">
      <div id="projects" className="container">
        <div className="text-center py-5">
          <h1>Projects</h1>
        </div>
        <Row className="mx-auto">
          {db.map((proj) => (
            <ProjectCard
              key={proj.id}
              name={proj.name}
              image={proj.image}
              text={proj.text}
              source={proj.source}
            />
          ))}
        </Row>
      </div>
    </div>
  );
}

export default Projects;
