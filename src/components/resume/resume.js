import React, { useEffect } from "react";
import "./resume.css";
import { Row, Col } from "react-bootstrap";
import { useInView } from "react-intersection-observer";
import { motion, useAnimation } from "framer-motion";
import BtNucamp from "../../assets/Bootstrap_Course_Completion.png";
import RENucamp from "../../assets/React_Course_Completion.png";
import RNNucamp from "../../assets/React_Native_Course_Completion.png";
import NENucamp from "../../assets/Node_Express_MongoDB_Course_Completion.png";

function Resume() {
  const animation = useAnimation();
  const [ref, inView] = useInView({ threshold: 0.1 });

  useEffect(() => {
    if (inView) {
      animation.start("visiable");
    } else {
      animation.start("hidden");
    }
  });

  return (
    <div id="resume" className="bgCont py-5">
      <motion.div
        ref={ref}
        animate={inView ? { opacity: 1 } : { opacity: 0 }}
        transition={{ duration: 1.2 }}
        initial="hidden"
        className="container rounded bg-white"
      >
        <Row>
          <Col lg="9" className="lg-mx-4">
            <Row>
              <p className="my-5 h1">Ronald Stewart</p>
              <p className="h5 intro">
                I am an aspiring web developer. I am seeking a web developer
                position with a growing company where I can learn and make a
                positive impact. I am eager to apply my knowledge in Vue, React,
                and Google Firebase as a professional developer position.
              </p>
              <p className="h5 pt-4 intro">
                I am also a nerd at heart. I love sci-fi, and anime. Some of my
                favorite shows are The Flash, The Expanse, Star Wars, Star Trek,
                Tenchi Muyo! War on Geminar, and The Rise of the Shield Hero.
              </p>
            </Row>
            <Row>
              <h1 className="my-4">Projects</h1>
              <h4>NuCamp React-Native</h4>
              <ul className="px-5">
                <li className="mb-2">
                  The project converts The React Course to a React-Native app
                  for Android and IOS.{" "}
                </li>
                <li>
                  It added features like adding favorites, using the Camera or
                  Gallery to upload your avatar, and using swap to delete your
                  favorites.
                </li>
              </ul>
              <h4>NuCamp React</h4>
              <ul className="px-5">
                <li className="mb-2">
                  This Project converts over the Bootstrap Course to use React,
                  React-Router, redux, and reactstrap.
                </li>
                <li>
                  It adds extra features like Comments to each Camp Site and
                  animation to the website.
                </li>
              </ul>
              <h4>NuCamp Bootstrap</h4>
              <ul className="px-5">
                <li className="mb-2">
                  A Website about Camping Location using Html, Css, and
                  Bootstrap. This was the first course from NuCamp.
                </li>
              </ul>
            </Row>
            <Row>
              <h1>Education</h1>
              <h4>
                NuCamp boot-Camp, Altant, Ga.- Full Stack Web Development course
              </h4>
              <p>
                <strong>April 2021 - September 2021</strong>
              </p>
              <h4>
                South Univerity, Savannh, Ga - Associate in Information
                Technoogy
              </h4>
              <p>
                <strong>2012 - 2014</strong>
              </p>
            </Row>
            <Row>
              <h1>Experince</h1>
              <h4>AutoZone, Mexia, Tx and Canton, Ga - Parts Sale Manger</h4>
              <p>
                <strong>October 2008 - PRESENT</strong>
              </p>
              <p>
                Open or Close the store, Supervise 2 to 6 employees, Cash
                Handling, Handle Customer's Concerns, and Inventory Managermnet
              </p>
            </Row>
          </Col>
          <Col className="ml-3">
            <h1 className="my-5">Skills</h1>
            <ul>
              <li className="lg-px-2 pb-3">Html, Css, React, React Native</li>
              <li className="lg-px-2 pb-3">Git, GitHub, Gitlab</li>
              <li className="lg-px-2 pb-3">Firebase, MongoDB</li>
            </ul>
            <h1 className="my-5 awards">Awards</h1>
            <ul className="list-unstyled d-sm-flex d-xl-block justify-content-between">
              <li>
                <img
                  className="NuCampImg pb-3"
                  src={BtNucamp}
                  alt="BootStrap Course"
                />
              </li>
              <li>
                <img
                  className="NuCampImg pb-3"
                  src={RENucamp}
                  alt="React Course"
                />
              </li>
              <li>
                <img
                  className="NuCampImg pb-3"
                  src={RNNucamp}
                  alt="React-Native Course"
                />
              </li>
              <li>
                <img
                  className="NuCampImg pb-3"
                  src={NENucamp}
                  alt="Express MongoDB Course"
                />
              </li>
            </ul>
            <h1 className="my-4">Languages</h1>
            <ul>
              <li>Html</li>
              <li>Css</li>
              <li>Javascript</li>
            </ul>
          </Col>
        </Row>
      </motion.div>
    </div>
  );
}

export default Resume;
