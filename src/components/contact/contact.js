import "./contact.css";
import React, { useEffect, useRef, useState } from "react";
import { Row, Col, Button, Form } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useInView } from "react-intersection-observer";
import { motion, useAnimation } from "framer-motion";
import emailjs from "emailjs-com";

function Contact() {
  const animation = useAnimation();
  const [ref, inView] = useInView({ threshold: 0.1 });
  const [done, setDone] = useState(false);

  const form = useRef();

  const sendEmail = (e) => {
    e.preventDefault();

    emailjs
      .sendForm(
        "service_0vzibmo",
        "template_7s6po5m",
        form.current,
        "user_89QugUoo8CyUWqJrudlfj"
      )
      .then(
        (result) => {
          console.log(result.text);
        },
        (error) => {
          console.log(error.text);
        }
      );
    form.current.reset();
    setDone(true);
  };

  useEffect(() => {
    if (inView) {
      animation.start("visible");
    } else {
      animation.start("hidden");
    }
  });

  return (
    <div id="contact" className="bgContact pt-5">
      <motion.div
        ref={ref}
        animate={inView ? { opacity: 1 } : { opacity: 0 }}
        transition={{ duration: 1.2 }}
        initial="hidden"
        className="container bg-white rounded"
      >
        <Row>
          <Col lg="4">
            <div className='contact-message'>
            <p className="h1 my-4 ">Let's Connect</p>
            <FontAwesomeIcon icon="handshake" size="9x" />
            </div>
            <ul className="contactUl">
              <li>
                <a
                  href="https://www.linkedin.com/in/ronald-stewart-6544808b"
                  target="_blank"
                  rel="noreferrer noopener"
                >
                  <FontAwesomeIcon
                    className="contIcon"
                    icon={["fab", "linkedin"]}
                  />
                  <div className="h4 d-none d-lg-block">Linkedin</div>
                </a>
              </li>
              <li>
                <a
                  href="https://twitter.com/RonaldS22601354"
                  target="_blank"
                  rel="noreferrer noopener"
                >
                  <FontAwesomeIcon
                    className="contIcon"
                    icon={["fab", "twitter"]}
                  />
                  <div className="h4 d-none d-lg-block">Twitter</div>
                </a>
              </li>
              <li>
                <a
                  href="https://gitlab.com/rstewart456"
                  target="_blank"
                  rel="noreferrer noopener"
                >
                  <FontAwesomeIcon
                    className="contIcon"
                    icon={["fab", "gitlab"]}
                  />
                  <div className="h4 d-none d-lg-block">GitLab</div>
                </a>
              </li>
            </ul>
          </Col>
          <Col className="pb-5">
            <div className="contact">Message Me</div>
            <Form ref={form} onSubmit={sendEmail}>
              <Form.Group className="mb-3" controlId="formGroupText">
                <Form.Label>Name</Form.Label>
                <Form.Control
                  name="user_name"
                  type="text"
                  placeholder="Enter Your Name"
                />
              </Form.Group>
              <Form.Group className="mb-3" controlId="formGroupEmail">
                <Form.Label>Email</Form.Label>
                <Form.Control
                  name="user_email"
                  type="email"
                  placeholder="Enter your Email Address"
                />
              </Form.Group>
              <Form.Group className="mb-3" controlId="formGroupText">
                <Form.Label>Message</Form.Label>
                <Form.Control
                  name="message"
                  as="textarea"
                  placeholder="Leave a Message"
                  style={{ height: "300px" }}
                />
              </Form.Group>
              <Button variant="primary" type="submit">
                Submit
              </Button>
              {done && "  Thank You..."}
            </Form>
          </Col>
        </Row>
      </motion.div>
      <div className="footer mt-5 d-flex align-items-center justify-content-between px-4">
        <div>MIT License</div>
        <div>Ronald Stewart</div>
      </div>
    </div>
  );
}

export default Contact;
