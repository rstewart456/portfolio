import "./home.css";
import React, { useEffect, useRef } from "react";
import avatar from "../../assets/me.jpeg";
import { Container, Row, Col } from "react-bootstrap";
import { useInView } from "react-intersection-observer";
import { motion, useAnimation } from "framer-motion";
import Typed from "typed.js";

function Home() {
  const textRef = useRef(null);
  const typed = useRef(null);
  const animation = useAnimation();
  const [ref, inView] = useInView({ threshold: 0.1 });

  useEffect(() => {
    if (inView) {
      animation.start("visible");
    } else {
      animation.start("hidden");
    }
    const options = {
      strings: ["Hello, My name is Ronald. I am a Web Developer"],
      showCursor: false,
      typeSpeed: 70,
      backSpeed: 40,
      backdelay: 20,
      loop: true,
    };
    typed.current = new Typed(textRef.current, options);

    return () => {
      typed.current.destroy();
    };
  }, [animation, inView]);

  return (
    <div id="home" className="homebg">
      <Container>
        <Row className="home-row">
          <Col
            md={6}
            className="d-md-flex justify-content-center align-items-center"
          >
            <motion.div
              ref={ref}
              animate={inView ? { opacity: 1 } : { opacity: 0 }}
              transition={{ duration: 1.2 }}
              initial="hidden"
            >
              <img
                className="homeImg d-flex justify-content-center align-items-center border border-dark rounded-circle"
                src={avatar}
                alt="avatar"
              />
            </motion.div>
          </Col>
          <Col
            md={6}
            className="d-md-flex justify-content-center align-items-center"
          >
            <span className="homeText" ref={textRef}></span>
          </Col>
        </Row>
      </Container>
    </div>
  );
}

export default Home;
